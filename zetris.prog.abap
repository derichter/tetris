REPORT zetris.

*https://codezentrale.de/tag/cl_gui_html_viewer/
*https://codezentrale.de/tag/cl_gui_timer/

CONSTANTS c_cmd_rotleft    TYPE string VALUE 'ROTLEFT'.
CONSTANTS c_cmd_left       TYPE string VALUE 'LEFT'.
CONSTANTS c_cmd_right      TYPE string VALUE 'RIGHT'.
CONSTANTS c_cmd_rotright   TYPE string VALUE 'ROTRIGHT'.
CONSTANTS c_cmd_restimer   TYPE string VALUE 'RESTIMER'.
CONSTANTS c_cmd_movedown   TYPE string VALUE 'MOVEDOWN'.

CONSTANTS c_game_height    TYPE i VALUE 600.
CONSTANTS c_game_width     TYPE i VALUE 300.
CONSTANTS c_preview_height TYPE i VALUE 180.
CONSTANTS c_preview_width  TYPE i VALUE 180.
CONSTANTS c_cell_height    TYPE i VALUE 30.
CONSTANTS c_cell_width     TYPE i VALUE 30.
CONSTANTS c_game_rows      TYPE i VALUE 20.
CONSTANTS c_game_cols      TYPE i VALUE 10.
CONSTANTS c_preview_rows   TYPE i VALUE 6.
CONSTANTS c_preview_cols   TYPE i VALUE 6.

TYPES: BEGIN OF ty_cell,
         value TYPE c LENGTH 1,
       END OF ty_cell,
       ty_unnamed_shape TYPE STANDARD TABLE OF ty_cell WITH NON-UNIQUE KEY value,
       BEGIN OF ty_shape,
         name TYPE c LENGTH 20,
         shape TYPE ty_unnamed_shape,
       END OF ty_shape,
       ty_shapes TYPE SORTED TABLE OF ty_shape WITH UNIQUE KEY name.

DATA c_shapes TYPE ty_shapes.
c_shapes = VALUE #( ( name = 'shape_l1'
                      shape = VALUE #( ( value = ' ' ) ( value = ' ' ) ( value = ' ' ) ( value = ' ' ) ( value = ' ' ) ( value = ' ' )
                                       ( value = ' ' ) ( value = ' ' ) ( value = 'X' ) ( value = ' ' ) ( value = ' ' ) ( value = ' ' )
                                       ( value = ' ' ) ( value = ' ' ) ( value = 'X' ) ( value = ' ' ) ( value = ' ' ) ( value = ' ' )
                                       ( value = ' ' ) ( value = ' ' ) ( value = 'X' ) ( value = 'X' ) ( value = ' ' ) ( value = ' ' )
                                       ( value = ' ' ) ( value = ' ' ) ( value = ' ' ) ( value = ' ' ) ( value = ' ' ) ( value = ' ' )
                                       ( value = ' ' ) ( value = ' ' ) ( value = ' ' ) ( value = ' ' ) ( value = ' ' ) ( value = ' ' ) ) )
                    ( name = 'shape_l2'
                      shape = VALUE #( ( value = ' ' ) ( value = ' ' ) ( value = ' ' ) ( value = ' ' ) ( value = ' ' ) ( value = ' ' )
                                       ( value = ' ' ) ( value = ' ' ) ( value = ' ' ) ( value = 'X' ) ( value = ' ' ) ( value = ' ' )
                                       ( value = ' ' ) ( value = ' ' ) ( value = ' ' ) ( value = 'X' ) ( value = ' ' ) ( value = ' ' )
                                       ( value = ' ' ) ( value = ' ' ) ( value = 'X' ) ( value = 'X' ) ( value = ' ' ) ( value = ' ' )
                                       ( value = ' ' ) ( value = ' ' ) ( value = ' ' ) ( value = ' ' ) ( value = ' ' ) ( value = ' ' )
                                       ( value = ' ' ) ( value = ' ' ) ( value = ' ' ) ( value = ' ' ) ( value = ' ' ) ( value = ' ' ) ) )
                    ( name = 'shape_s1'
                      shape = VALUE #( ( value = ' ' ) ( value = ' ' ) ( value = ' ' ) ( value = ' ' ) ( value = ' ' ) ( value = ' ' )
                                       ( value = ' ' ) ( value = ' ' ) ( value = 'X' ) ( value = ' ' ) ( value = ' ' ) ( value = ' ' )
                                       ( value = ' ' ) ( value = ' ' ) ( value = 'X' ) ( value = 'X' ) ( value = ' ' ) ( value = ' ' )
                                       ( value = ' ' ) ( value = ' ' ) ( value = ' ' ) ( value = 'X' ) ( value = ' ' ) ( value = ' ' )
                                       ( value = ' ' ) ( value = ' ' ) ( value = ' ' ) ( value = ' ' ) ( value = ' ' ) ( value = ' ' )
                                       ( value = ' ' ) ( value = ' ' ) ( value = ' ' ) ( value = ' ' ) ( value = ' ' ) ( value = ' ' ) ) )
                    ( name = 'shape_s2'
                      shape = VALUE #( ( value = ' ' ) ( value = ' ' ) ( value = ' ' ) ( value = ' ' ) ( value = ' ' ) ( value = ' ' )
                                       ( value = ' ' ) ( value = ' ' ) ( value = ' ' ) ( value = 'X' ) ( value = ' ' ) ( value = ' ' )
                                       ( value = ' ' ) ( value = ' ' ) ( value = 'X' ) ( value = 'X' ) ( value = ' ' ) ( value = ' ' )
                                       ( value = ' ' ) ( value = ' ' ) ( value = 'X' ) ( value = ' ' ) ( value = ' ' ) ( value = ' ' )
                                       ( value = ' ' ) ( value = ' ' ) ( value = ' ' ) ( value = ' ' ) ( value = ' ' ) ( value = ' ' )
                                       ( value = ' ' ) ( value = ' ' ) ( value = ' ' ) ( value = ' ' ) ( value = ' ' ) ( value = ' ' ) ) )
                    ( name = 'shape_t'
                      shape = VALUE #( ( value = ' ' ) ( value = ' ' ) ( value = ' ' ) ( value = ' ' ) ( value = ' ' ) ( value = ' ' )
                                       ( value = ' ' ) ( value = ' ' ) ( value = 'X' ) ( value = ' ' ) ( value = ' ' ) ( value = ' ' )
                                       ( value = ' ' ) ( value = ' ' ) ( value = 'X' ) ( value = 'X' ) ( value = ' ' ) ( value = ' ' )
                                       ( value = ' ' ) ( value = ' ' ) ( value = 'X' ) ( value = ' ' ) ( value = ' ' ) ( value = ' ' )
                                       ( value = ' ' ) ( value = ' ' ) ( value = ' ' ) ( value = ' ' ) ( value = ' ' ) ( value = ' ' )
                                       ( value = ' ' ) ( value = ' ' ) ( value = ' ' ) ( value = ' ' ) ( value = ' ' ) ( value = ' ' ) ) )
                    ( name = 'shape_i'
                      shape = VALUE #( ( value = ' ' ) ( value = ' ' ) ( value = ' ' ) ( value = ' ' ) ( value = ' ' ) ( value = ' ' )
                                       ( value = ' ' ) ( value = ' ' ) ( value = 'X' ) ( value = ' ' ) ( value = ' ' ) ( value = ' ' )
                                       ( value = ' ' ) ( value = ' ' ) ( value = 'X' ) ( value = ' ' ) ( value = ' ' ) ( value = ' ' )
                                       ( value = ' ' ) ( value = ' ' ) ( value = 'X' ) ( value = ' ' ) ( value = ' ' ) ( value = ' ' )
                                       ( value = ' ' ) ( value = ' ' ) ( value = 'X' ) ( value = ' ' ) ( value = ' ' ) ( value = ' ' )
                                       ( value = ' ' ) ( value = ' ' ) ( value = ' ' ) ( value = ' ' ) ( value = ' ' ) ( value = ' ' ) ) )
                    ( name = 'shape_o'
                      shape = VALUE #( ( value = ' ' ) ( value = ' ' ) ( value = ' ' ) ( value = ' ' ) ( value = ' ' ) ( value = ' ' )
                                       ( value = ' ' ) ( value = ' ' ) ( value = ' ' ) ( value = ' ' ) ( value = ' ' ) ( value = ' ' )
                                       ( value = ' ' ) ( value = ' ' ) ( value = 'X' ) ( value = 'X' ) ( value = ' ' ) ( value = ' ' )
                                       ( value = ' ' ) ( value = ' ' ) ( value = 'X' ) ( value = 'X' ) ( value = ' ' ) ( value = ' ' )
                                       ( value = ' ' ) ( value = ' ' ) ( value = ' ' ) ( value = ' ' ) ( value = ' ' ) ( value = ' ' )
                                       ( value = ' ' ) ( value = ' ' ) ( value = ' ' ) ( value = ' ' ) ( value = ' ' ) ( value = ' ' ) ) ) ).

TYPES: BEGIN OF ty_rotation,
         name TYPE c LENGTH 20,
         rotation TYPE i,
       END OF ty_rotation,
       ty_rotations TYPE SORTED TABLE OF ty_rotation WITH UNIQUE KEY name.

DATA c_rotations TYPE ty_rotations.
c_rotations = VALUE #( ( name = '0°'
                         rotation = 0 )
                       ( name = '90°'
                         rotation = 1 )
                       ( name = '180°'
                         rotation = 2 )
                       ( name = '270°'
                         rotation = 3 ) ).

CONSTANTS c_color_red    TYPE string VALUE 'RED'.
CONSTANTS c_color_green  TYPE string VALUE 'GREEN'.
CONSTANTS c_color_blue   TYPE string VALUE 'BLUE'.
CONSTANTS c_color_yellow TYPE string VALUE 'YELLOW'.
CONSTANTS c_color_purple TYPE string VALUE 'PURPLE'.
CONSTANTS c_color_orange TYPE string VALUE 'ORANGE'.

TYPES: BEGIN OF ty_color,
         name TYPE c LENGTH 20,
         color TYPE string,
       END OF ty_color,
       ty_colors TYPE SORTED TABLE OF ty_color WITH UNIQUE KEY name.

DATA c_colors TYPE ty_colors.
c_colors = VALUE #( ( name = 'red'
                      color = c_color_red    )
                    ( name = 'green'
                      color = c_color_green )
                    ( name = 'blue'
                      color = c_color_blue )
                    ( name = 'yellow'
                      color = c_color_yellow )
                    ( name = 'purple'
                      color = c_color_purple )
                    ( name = 'orange'
                      color = c_color_orange ) ).

CLASS lcl_game DEFINITION.
  PUBLIC SECTION.
    METHODS: constructor.

*    lifecycle methods
    METHODS: start.

    METHODS: on_sapevent FOR EVENT sapevent OF cl_gui_html_viewer " called when the user presses a button on the html page
      IMPORTING
        action
        frame
        getdata
        postdata
        query_table
        sender.

    METHODS: on_game_increment FOR EVENT finished OF cl_gui_timer " called when the timer finishes
      IMPORTING
        sender.

    METHODS: finish.

*    actions
    METHODS: move_up.

    METHODS: move_down.

    METHODS: move_until_end.

    METHODS: move_left.

    METHODS: move_right.

    METHODS: rotate_left.

    METHODS: rotate_right.

*    helper
    METHODS: new_figure.

    METHODS: remove_completed_rows.

    METHODS: write_figure_to_game.

    METHODS: check_overlap
      EXPORTING
        ev_overlap TYPE abap_bool.

    METHODS: generate_html.

  PRIVATE SECTION.
*    member class instances
    DATA: mo_html           TYPE REF TO cl_gui_html_viewer.
    DATA: mo_timer          TYPE REF TO cl_gui_timer.
    DATA: mo_random         TYPE REF TO cl_abap_random.

*    member html variables
    DATA: mt_html           TYPE html_table.
    DATA: mv_url            TYPE skwf_url.

*    game state variables
    DATA: mv_finished       TYPE abap_bool VALUE abap_false. " true if the user cannot move any more
    DATA: mt_game_state     TYPE TABLE OF string. " array containing the color of all cells
    DATA: mt_current_figure TYPE TABLE OF string. " array containing the structurce of the current figure
    DATA: mv_offset_top     TYPE i VALUE 0. " offset for printing the current figure to the game state
    DATA: mv_offset_left    TYPE i VALUE 0. " offset for printing the current figure to the game state
    DATA: mv_rotation       TYPE i VALUE 0. " rotation of the current figure 0,1,2 or 3 (0°,90°,180° or 270°)
    DATA: mv_color          TYPE string VALUE c_color_blue. " color of the current figure
ENDCLASS.

CLASS lcl_game IMPLEMENTATION.
  METHOD constructor.
    mo_html = NEW cl_gui_html_viewer( parent = cl_gui_container=>default_screen ).
    SET HANDLER me->on_sapevent FOR mo_html.
    mo_html->set_registered_events( events = VALUE #( ( eventid    = cl_gui_html_viewer=>m_id_sapevent
                                                        appl_event = abap_true ) ) ).

    mo_timer = NEW cl_gui_timer( ).
    SET HANDLER me->on_game_increment FOR mo_timer.
    mo_timer->interval = 1.

    mo_random = cl_abap_random=>create( cl_abap_random=>seed( ) ).
  ENDMETHOD.

*  lifecycle methods
  METHOD start.
*    initial setup of the game state
    mv_finished = abap_false.
    DO ( c_game_rows * c_game_cols ) TIMES.
      mt_game_state = VALUE #( BASE mt_game_state
                               ( ) ).
    ENDDO.

    me->new_figure( ).

    me->generate_html( ).

*    start timer
    mo_timer->run( ).
  ENDMETHOD.

  METHOD on_sapevent.
*    TODO: find a workaround for this
    IF action = c_cmd_restimer.
      mo_timer->run( ).
      EXIT.
    ENDIF.

*    user decided to move this item as far as it can go
    IF action = c_cmd_movedown.
      me->move_until_end( ).

      me->write_figure_to_game( ).

      me->remove_completed_rows( ).

      me->new_figure( ).

      me->generate_html( ).

      EXIT. " early release: we dont need to do the checking in this case
    ENDIF.

*    user made a move
    CASE action.
      WHEN c_cmd_rotleft.
        me->rotate_left( ).
      WHEN c_cmd_left.
        me->move_left( ).
      WHEN c_cmd_right.
        me->move_right( ).
      WHEN c_cmd_rotright.
        me->rotate_right( ).
      WHEN OTHERS.
    ENDCASE.

    me->check_overlap(
      IMPORTING
        ev_overlap = DATA(lv_overlap)
    ).
    IF lv_overlap IS NOT INITIAL.
*      undo ther user move
      CASE action.
        WHEN c_cmd_rotleft.
          me->rotate_right( ).
        WHEN c_cmd_left.
          me->move_right( ).
        WHEN c_cmd_right.
          me->move_left( ).
        WHEN c_cmd_rotright.
          me->rotate_left( ).
        WHEN OTHERS.
      ENDCASE.
    ENDIF.

    me->generate_html( ).
  ENDMETHOD.

  METHOD on_game_increment.
*    time dependent update of the game state
    me->move_down( ).

    me->check_overlap(
      IMPORTING
        ev_overlap = DATA(lv_overlap)
    ).
    IF lv_overlap IS NOT INITIAL.
*      undo and startover
      me->move_up( ).

*      check if the game is over, of if we can play on
      IF mv_offset_top <= 1.
        me->finish( ).
        EXIT.
      ENDIF.

      me->write_figure_to_game( ).

      me->remove_completed_rows( ).

      me->new_figure( ).
    ENDIF.

    me->generate_html( ).

*    time increment
    sender->run( ).
  ENDMETHOD.

  METHOD finish.
*   finishing the game state
    mv_finished = abap_true.
    mt_game_state = VALUE #( ).
    DO ( c_game_rows * c_game_cols ) TIMES.
      mt_game_state = VALUE #( BASE mt_game_state
                               ( c_color_blue ) ).
    ENDDO.

    me->generate_html( ).
  ENDMETHOD.

*  actions
  METHOD move_up.
    mv_offset_top = mv_offset_top - 1.
  ENDMETHOD.

  METHOD move_down.
    mv_offset_top = mv_offset_top + 1.
  ENDMETHOD.

  METHOD move_until_end.
    DATA: lv_i TYPE i VALUE 0.
    DATA: lv_j TYPE i VALUE 0.
    DATA: lv_i_t TYPE i VALUE 0.
    DATA: lv_j_t TYPE i VALUE 0.
    DATA: lv_index TYPE i VALUE 0.
    DATA: lv_skip TYPE i.

    DO ( c_preview_rows * c_preview_cols ) TIMES.
*      early release: only need to process figure cells
      IF mt_current_figure[ sy-index ] IS INITIAL.
        CONTINUE.
      ENDIF.

*      get indexes
      lv_i = ( sy-index - 1 ) DIV c_preview_cols.
      lv_j = ( sy-index - 1 ) MOD c_preview_cols.

*      get transformed indexes
      IF mv_rotation = 0.
        lv_i_t = lv_i.
        lv_j_t = lv_j.
      ELSEIF mv_rotation = 1.
        lv_i_t = lv_j.
        lv_j_t = ( c_preview_cols - 1 ) - lv_i.
      ELSEIF mv_rotation = 2.
        lv_i_t = ( c_preview_rows - 1 ) - lv_i.
        lv_j_t = ( c_preview_cols - 1 ) - lv_j.
      ELSEIF mv_rotation = 3.
        lv_i_t = ( c_preview_cols - 1 ) - lv_j.
        lv_j_t = lv_i.
      ENDIF.
      lv_i = lv_i_t.
      lv_j = lv_j_t.

*      get indexes with offset
      lv_i = lv_i + mv_offset_top.
      lv_j = lv_j + mv_offset_left.

      WHILE abap_true IS NOT INITIAL.
        lv_i = lv_i + 1.

        lv_index = lv_i * c_game_cols + lv_j + 1.
        IF 0 > lv_i OR lv_i >= c_game_rows OR
           0 > lv_j OR lv_j >= c_game_cols OR
           mt_game_state[ lv_index ] IS NOT INITIAL.

          lv_index = sy-index.
          EXIT.
        ENDIF.
      ENDWHILE.
      IF lv_skip IS INITIAL OR lv_skip > lv_index - 1.
        lv_skip = lv_index - 1.
      ENDIF.
    ENDDO.

    mv_offset_top = mv_offset_top + lv_skip.
  ENDMETHOD.

  METHOD move_left.
    mv_offset_left = mv_offset_left - 1.
  ENDMETHOD.

  METHOD move_right.
    mv_offset_left = mv_offset_left + 1.
  ENDMETHOD.

  METHOD rotate_left.
    mv_rotation = mv_rotation - 1.
    IF mv_rotation < 0.
      mv_rotation = 3.
    ENDIF.
  ENDMETHOD.

  METHOD rotate_right.
    mv_rotation = mv_rotation + 1.
    IF mv_rotation > 3.
      mv_rotation = 0.
    ENDIF.
  ENDMETHOD.

*  helper
  METHOD new_figure.
    mv_offset_top     = 0.
    mv_offset_left    = ( c_game_cols - c_preview_cols ) DIV 2.
    mt_current_figure = c_shapes[    mo_random->intinrange( low = 1 high = lines( c_shapes ) )    ]-shape.
    mv_rotation       = c_rotations[ mo_random->intinrange( low = 1 high = lines( c_rotations ) ) ]-rotation.
    mv_color          = c_colors[    mo_random->intinrange( low = 1 high = lines( c_colors ) )    ]-color.
  ENDMETHOD.

  METHOD remove_completed_rows.

  ENDMETHOD.

  METHOD write_figure_to_game.
    DATA: lv_i TYPE i VALUE 0.
    DATA: lv_j TYPE i VALUE 0.
    DATA: lv_i_t TYPE i VALUE 0.
    DATA: lv_j_t TYPE i VALUE 0.
    DATA: lv_index TYPE i VALUE 0.

    DO ( c_preview_rows * c_preview_cols ) TIMES.
*      early release: only need to process figure cells
      IF mt_current_figure[ sy-index ] IS INITIAL.
        CONTINUE.
      ENDIF.

*      get indexes
      lv_i = ( sy-index - 1 ) DIV c_preview_cols.
      lv_j = ( sy-index - 1 ) MOD c_preview_cols.

*      get transformed indexes
      IF mv_rotation = 0.
        lv_i_t = lv_i.
        lv_j_t = lv_j.
      ELSEIF mv_rotation = 1.
        lv_i_t = lv_j.
        lv_j_t = ( c_preview_cols - 1 ) - lv_i.
      ELSEIF mv_rotation = 2.
        lv_i_t = ( c_preview_rows - 1 ) - lv_i.
        lv_j_t = ( c_preview_cols - 1 ) - lv_j.
      ELSEIF mv_rotation = 3.
        lv_i_t = ( c_preview_cols - 1 ) - lv_j.
        lv_j_t = lv_i.
      ENDIF.
      lv_i = lv_i_t.
      lv_j = lv_j_t.

*      get indexes with offset
      lv_i = lv_i + mv_offset_top.
      lv_j = lv_j + mv_offset_left.

*      set game state value
      lv_index = lv_i * c_game_cols + lv_j + 1.
      IF 0 <= lv_i AND lv_i < c_game_rows AND
         0 <= lv_j AND lv_j < c_game_cols.

        mt_game_state[ lv_index ] = mv_color.
      ENDIF.
    ENDDO.
  ENDMETHOD.

  METHOD check_overlap.
    DATA: lv_i TYPE i VALUE 0.
    DATA: lv_j TYPE i VALUE 0.
    DATA: lv_i_t TYPE i VALUE 0.
    DATA: lv_j_t TYPE i VALUE 0.
    DATA: lv_index TYPE i VALUE 0.

    DO ( c_preview_rows * c_preview_cols ) TIMES.
*      early release: only need to process figure cells
      IF mt_current_figure[ sy-index ] IS INITIAL.
        CONTINUE.
      ENDIF.

*      get indexes
      lv_i = ( sy-index - 1 ) DIV c_preview_cols.
      lv_j = ( sy-index - 1 ) MOD c_preview_cols.

*      get transformed indexes
      IF mv_rotation = 0.
        lv_i_t = lv_i.
        lv_j_t = lv_j.
      ELSEIF mv_rotation = 1.
        lv_i_t = lv_j.
        lv_j_t = ( c_preview_cols - 1 ) - lv_i.
      ELSEIF mv_rotation = 2.
        lv_i_t = ( c_preview_rows - 1 ) - lv_i.
        lv_j_t = ( c_preview_cols - 1 ) - lv_j.
      ELSEIF mv_rotation = 3.
        lv_i_t = ( c_preview_cols - 1 ) - lv_j.
        lv_j_t = lv_i.
      ENDIF.
      lv_i = lv_i_t.
      lv_j = lv_j_t.

*      get indexes with offset
      lv_i = lv_i + mv_offset_top.
      lv_j = lv_j + mv_offset_left.

*      check game state value
      lv_index = lv_i * c_game_cols + lv_j + 1.
      IF 0 > lv_i OR lv_i >= c_game_rows OR
         0 > lv_j OR lv_j >= c_game_cols OR
         mt_game_state[ lv_index ] IS NOT INITIAL.

        ev_overlap = abap_true.
        EXIT. " early release: we only need to find one overlap
      ENDIF.
    ENDDO.
  ENDMETHOD.

  METHOD generate_html.
    DATA: lv_i TYPE i VALUE 0.
    DATA: lv_j TYPE i VALUE 0.
    DATA: lv_i_t TYPE i VALUE 0.
    DATA: lv_j_t TYPE i VALUE 0.
    DATA: lv_index TYPE i VALUE 0.

    mt_html = VALUE #(
      ( |<html>                                                                                                 | )
      ( |  <head>                                                                                               | )
      ( |    <meta http-equiv="content-type" content="text/html; charset=utf-8">                                | )
      ( |    <script type="text/javascript">                                                                    | )
      ( |      document.body.onkeydown = function(event) \{                                                     | )
      ( |        alert(String.fromCharCode(event.keyCode)+" --> " + event.keyCode);                             | )
      ( |        location.href = "SAPEVENT:" + element;                                                         | )
      ( |      \};                                                                                              | )
      ( |      function onKeyDown(element) \{                                                                   | )
      ( |        location.href = "SAPEVENT:" + element;                                                         | )
      ( |      \};                                                                                              | )
      ( |    </script>                                                                                          | )
      ( |    <style>                                                                                            | )
      ( |        body \{                                                                                        | )
      ( |            margin: 0px;                                                                               | )
      ( |        \}                                                                                             | )
      ( |        .wrapper \{                                                                                    | )
      ( |            height: 100%;                                                                              | )
      ( |            width: 100%;                                                                               | )
      ( |            display: flex;                                                                             | )
      ( |            justify-content: center;                                                                   | )
      ( |            align-items: center;                                                                       | )
      ( |        \}                                                                                             | )
      ( |        .leftContainer \{                                                                              | )
      ( |            height: 100%;                                                                              | )
      ( |            width: { c_game_width }px;                                                                 | )
      ( |            margin: 20px;                                                                              | )
      ( |        \}                                                                                             | )
      ( |        .rightContainer \{                                                                             | )
      ( |            height: 100%;                                                                              | )
      ( |            width: { c_preview_width }px;                                                              | )
      ( |            margin: 20px;                                                                              | )
      ( |        \}                                                                                             | )
      ( |        .gameContainer \{                                                                              | )
      ( |            height: { c_game_height }px;                                                               | )
      ( |            width: { c_game_width }px;                                                                 | )
      ( |            display: flex;                                                                             | )
      ( |            flex-wrap: wrap;                                                                           | )
      ( |        \}                                                                                             | )
      ( |        .buttonContainer \{                                                                            | )
      ( |            height: fit-content;                                                                       | )
      ( |            width: 100%;                                                                               | )
      ( |            padding-top: 10px;                                                                         | )
      ( |            display: flex;                                                                             | )
      ( |            justify-content: center;                                                                   | )
      ( |        \}                                                                                             | )
      ( |        .previewContainer \{                                                                           | )
      ( |            height: { c_preview_height }px;                                                            | )
      ( |            width: { c_preview_width }px;                                                              | )
      ( |            display: flex;                                                                             | )
      ( |            flex-wrap: wrap;                                                                           | )
      ( |        \}                                                                                             | )
      ( |        .cell \{                                                                                       | )
      ( |            height: { c_cell_height }px;                                                               | )
      ( |            width: { c_cell_width }px;                                                                 | )
      ( |            background-color: rgb(77, 77, 77);                                                         | )
      ( |            box-shadow: inset 0 0 1em rgb(186, 186, 186);                                              | )
      ( |            display: flex;                                                                             | )
      ( |        \}                                                                                             | )
      ( |        .{ c_color_red } \{                                                                                        | )
      ( |            background-color: rgb(245, 13, 13);                                                        | )
      ( |        \}                                                                                             | )
      ( |        .{ c_color_green } \{                                                                                      | )
      ( |            background-color: rgb(23, 201, 20);                                                        | )
      ( |        \}                                                                                             | )
      ( |        .{ c_color_blue } \{                                                                                       | )
      ( |            background-color: rgb(20, 189, 201);                                                       | )
      ( |        \}                                                                                             | )
      ( |        .{ c_color_yellow } \{                                                                                     | )
      ( |            background-color: rgb(255, 239, 62);                                                       | )
      ( |        \}                                                                                             | )
      ( |        .{ c_color_purple } \{                                                                                     | )
      ( |            background-color: rgb(162, 20, 201);                                                       | )
      ( |        \}                                                                                             | )
      ( |        .{ c_color_orange } \{                                                                                     | )
      ( |            background-color: rgb(255, 162, 0);                                                        | )
      ( |        \}                                                                                             | )
      ( |    </style>                                                                                           | )
      ( |  </head>                                                                                              | )
      ( |  <body>                                                                                               | )
      ( |    <div class="wrapper">                                                                              | )
      ( |      <div class="leftContainer">                                                                      | )
      ( |        <div class="gameContainer">                                                                    | )
    ).

    DO ( c_game_rows * c_game_cols ) TIMES.
*      get indexes
      lv_i = ( sy-index - 1 ) DIV c_game_cols.
      lv_j = ( sy-index - 1 ) MOD c_game_cols.

*      get indexes with offset
      lv_i = lv_i - mv_offset_top.
      lv_j = lv_j - mv_offset_left.

*      get transformed indexes
      IF mv_rotation = 0.
        lv_i_t = lv_i.
        lv_j_t = lv_j.
      ELSEIF mv_rotation = 1.
        lv_i_t = ( c_preview_rows - 1 ) - lv_j.
        lv_j_t = lv_i.
      ELSEIF mv_rotation = 2.
        lv_i_t = ( c_preview_rows - 1 ) - lv_i.
        lv_j_t = ( c_preview_cols - 1 ) - lv_j.
      ELSEIF mv_rotation = 3.
        lv_i_t = lv_j.
        lv_j_t = ( c_preview_cols - 1 ) - lv_i.
      ENDIF.
      lv_i = lv_i_t.
      lv_j = lv_j_t.

*      get game state value
      lv_index = lv_i * c_preview_cols + lv_j + 1.
      IF 0 <= lv_i AND lv_i < c_preview_rows AND
         0 <= lv_j AND lv_j < c_preview_cols AND
         line_exists( mt_current_figure[ lv_index ] ) AND mt_current_figure[ lv_index ] IS NOT INITIAL.
        mt_html = VALUE #( BASE mt_html
          ( |      <div class="cell { mv_color }"></div>                                                                     | )
        ).
      ELSEIF mt_game_state[ sy-index ] IS NOT INITIAL.
        mt_html = VALUE #( BASE mt_html
          ( |      <div class="cell { mt_game_state[ sy-index ] }"></div>                                                                     | )
        ).
      ELSE.
        mt_html = VALUE #( BASE mt_html
          ( |      <div class="cell"></div>                                                                     | )
        ).
      ENDIF.
    ENDDO.

    mt_html = VALUE #( BASE mt_html
      ( |        </div>                                                                                         | )
      ( |        <div class="buttonContainer">                                                                  | )
      ( |          <button onClick="onKeyDown('{ c_cmd_rotleft }')">Rotate Left</button>                                  | )
      ( |          <button onClick="onKeyDown('{ c_cmd_left }')">Left</button>                                  | )
      ( |          <button onClick="onKeyDown('{ c_cmd_right }')">Right</button>                                | )
      ( |          <button onClick="onKeyDown('{ c_cmd_rotright }')">Rotate Right</button>                                | )
      ( |        </div>                                                                                         | )
      ( |        <div class="buttonContainer">                                                                  | )
      ( |          <button onClick="onKeyDown('{ c_cmd_restimer }')">Restart Timer</button>                                | )
      ( |          <button onClick="onKeyDown('{ c_cmd_movedown }')">Move Down</button>                                | )
      ( |        </div>                                                                                         | )
      ( |      </div>                                                                                           | )
      ( |      <div class="rightContainer">                                                                     | )
      ( |        <div class="previewContainer">                                                                 | )
    ).

    DO ( c_preview_rows * c_preview_cols ) TIMES.
*      get indexes
      lv_i = ( sy-index - 1 ) DIV c_preview_cols.
      lv_j = ( sy-index - 1 ) MOD c_preview_cols.

*      get transformed indexes
      IF mv_rotation = 0.
        lv_i_t = lv_i.
        lv_j_t = lv_j.
      ELSEIF mv_rotation = 1.
        lv_i_t = ( c_preview_rows - 1 ) - lv_j.
        lv_j_t = lv_i.
      ELSEIF mv_rotation = 2.
        lv_i_t = ( c_preview_rows - 1 ) - lv_i.
        lv_j_t = ( c_preview_cols - 1 ) - lv_j.
      ELSEIF mv_rotation = 3.
        lv_i_t = lv_j.
        lv_j_t = ( c_preview_cols - 1 ) - lv_i.
      ENDIF.
      lv_i = lv_i_t.
      lv_j = lv_j_t.

*      get preview value
      lv_index = lv_i * c_preview_cols + lv_j + 1.
      IF 0 <= lv_i AND lv_i < c_preview_rows AND
         0 <= lv_j AND lv_j < c_preview_cols AND
         mt_current_figure[ lv_index ] IS NOT INITIAL.

        mt_html = VALUE #( BASE mt_html
          ( |      <div class="cell { mv_color }"></div>                                                                     | )
        ).
      ELSE.
        mt_html = VALUE #( BASE mt_html
          ( |      <div class="cell"></div>                                                                     | )
        ).
      ENDIF.
    ENDDO.

    mt_html = VALUE #( BASE mt_html
      ( |        </div>                                                                                         | )
      ( |      </div>                                                                                           | )
      ( |    </div>                                                                                             | )
      ( |  </body>                                                                                              | )
      ( |</html>                                                                                                | )
    ).

    mo_html->load_data( IMPORTING
                          assigned_url = mv_url
                        CHANGING
                          data_table   = mt_html ).

    mo_html->show_url( url = mv_url ).
  ENDMETHOD.
ENDCLASS.

SELECTION-SCREEN BEGIN OF SCREEN 2000.
SELECTION-SCREEN END OF SCREEN 2000.

START-OF-SELECTION.
  DATA(go_game) = NEW lcl_game( ).
  go_game->start( ).

  CALL SCREEN 2000.
